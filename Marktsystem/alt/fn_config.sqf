/*
##################### DYNAMIC MARKET SCRIPT #####################
### AUTHOR: RYAN TT.                                          ###
### STEAM: www.steamcommunity.com/id/ryanthett                ###
###                                                           ###
### DISCLAIMER: THIS SCRIPT CAN BE USED ON EVERY SERVER ONLY  ###
###             WITH THIS HEADER / NOTIFICATION               ###
#################################################################
*/

// ███████████████████████████████████████████████████████████████████████
// █████████████████ DYNAMIC MARKET BASIC CONFIGURATION ██████████████████
// ███████████████████████████████████████████████████████████████████████

DYNMARKET_Serveruptime         = 04;   // Serveruptime after restart in hours
DYNMARKET_UseExternalDatabase  = true; // Should the script use the External Database?
DYNMARKET_PriceUpdateInterval  = 15;   // After how many minutes should the price be updated?
DYNMARKET_CreateBackups        = true; // Should the server save write the prices regulary into the Database? If false, it will save the prices before Server-restart?
DYNMARKET_CreateBackupInterval = 01;   // After how many updates (PriceUpdateIntervals) should the prices be saved into the Database?
DYNMARKET_UserNotification     = true; // Should the user be informed with a hint whenever the prices got updated?

// █████████████████ USER NOTIFICATION TEXTS  █████████████████

DYNMARKET_UserNotification_Text = 
[
	"Die Preise wurden soeben Aktualisiert!",
	"Der Server berechnet die neuen Preise..."
];

// █████████████████ ITEM GROUP CONFIGURATION █████████████████

DYNMARKET_Items_Groups =
[
	["nonChange",
		[
			["water",-1,15,15],
			["rabbit",-1,45,45],
			["coffee",-1,10,10],
			["turtlesoup",-1,150,150],
			["fuelE",-1,200,200],
			["fuelF",-1,400,400],
			["pickaxe",-1,600,600],
			["tbacon",-1,60,60],
			["lockpick",-1,2500,2500],
			["redgull",-1,750,750],
			["spikeStrip",-1,600,600],
			["gagkit",-1,10000,10000],
			["goldbar",-1,309000,309000],
			["blastingcharge",-1,10000,10000],
			["boltcutter",-1,75000,75000],
			["defusekit",-1,200,200],
			["storagesmall",-1,75000,75000],
			["storagebig",-1,125000,125000],
			["hammer",-1,200,200],
			["emptywater",-1,5,5],
			["metallschrott",-1,25,25],
			["smara",-1,500000,500000],
			["fahrzeugteil",-1,100,100],
			["yeast",-1,100,100],
			["puranium",-1,750,750],
			["uranium1",-1,1000,1000],
			["uranium2",-1,2000,2000],
			["uranium3",-1,3000,3000],
			["uranium4",-1,4000,4000],
			["oilu",-1,600,600],
			["cannabis",-1,600,600],
			["ironore",-1,400,400],
			["copperore",-1,400,400],
			["salt",-1,400,400],
			["sand",-1,400,400],
			["diamond",-1,400,400],
			["rock",-1,400,400],
			["coal",-1,400,400],
			["silver",-1,400,400],
			["gold",-1,400,400],
			["zinnu",-1,400,400],
			["rye",-1,120,120],
			["hops",-1,440,440],
			["cornmeal",-1,400,400],
			["beerp",-1,100,100],
			["whiskey",-1,200,200],
			["mash",-1,160,160],
			["corn",-1,140,140],
			["hen_raw",-1,80,80],
			["rooster_raw",-1,90,90],
			["goat_raw",-1,130,130],
			["sheep_raw",-1,600,600],
			["rabbit_raw",-1,80,80],
			["frog",-1,500,500],
			["moonshine",-1,900,900],
			["cocaine",-1,1000,1000],
			["heroinu",-1,1100,1100]
		],
		0
	],
	["illegal", 
		[
			["heroinp",-1,2500,20000],
			["marijuana",-1,2000,20000],
			["turtle",-1,3000,30000],
			["catshark",-1,1200,15000],
			["cocainep",-1,2500,20000],
			["bottledshine",-1,800,20000],
			["froglegs",-1,350,2000],
			["froglsd",-1,2000,20000]
		],
		0.25
	],
	["legal", 
		[
			["oilp",-1,500,16500],
			["apple",-1,25,50],
			["peach",-1,60,90],
			["iron_r",-1,500,16500],
			["copper_r",-1,1500,16500],
			["salt_r",-1,500,16500],
			["glass",-1,500,16500],
			["diamondc",-1,500,17500],
			["cement",-1,500,16500],
			["coalr",-1,500,16500],
			["silverr",-1,500,16500],
			["goldr",-1,500,16500],
			["saft",-1,70,550],
			["zinnp",-1,500,16500],
			["bottles",-1,400,2700],
			["bread",-1,500,3000],
			["hen_grilled",-1,30,220],
			["rooster_grilled",-1,25,160],
			["goat_grilled",-1,50,840],
			["sheep_grilled",-1,120,1600],
			["rabbit_grilled",-1,40,400],
			["strick",-1,500,13500]
		],
		0.05
	],
	["legalcombi", 
		[
			["uranium",-1,5000,40000],
			["silberschmuck",-1,5000,17000],
			["goldschmuck",-1,5000,20000],
			["weissgold",-1,5000,16000],
			["stahl",-1,3500,15000],
			["bronze",-1,3500,15000],
			["bottledbeer",-1,1200,10000],
			["bottledwhiskey",-1,1400,12000]
		],
		0.1
	],
	["fische", 
		[
			["salema",-1,200,5000],
			["ornate",-1,200,5000],
			["mackerel",-1,200,5000],
			["tuna",-1,200,5000],
			["mullet",-1,200,1000]
		],
		0.1
	]
];

// █████████████████    ALL SELLABLE ITEMS    █████████████████

DYNMARKET_Items_ToTrack        = 
[
	["oilu",600],
	["oilp",9500],
	["heroinu",700],
	["heroinp",11780],
	["cannabis",300],
	["marijuana",4700],
	["apple",25],
	["water",15],
	["rabbit",45],
	["salema",400],
	["ornate",550],
	["mackerel",800],
	["tuna",2100],
	["mullet",1000],
	["catshark",12500],
	["turtle",19500],
	["coffee",10],
	["turtlesoup",150],
	["donuts",30],
	["fuelE",200],
	["fuelF",400],
	["pickaxe",600],
	["copperore",230],
	["ironore",290],
	["iron_r",1150],
	["copper_r",1280],
	["salt",260],
	["salt_r",1000],
	["sand",270],
	["glass",1800],
	["tbacon",60],
	["lockpick",2500],
	["redgull",750],
	["peach",60],
	["diamond",900],
	["diamondc",4900],
	["cocaine",500],
	["cocainep",12200],
	["spikeStrip",600],
	["cement",1360],
	["rock",200],
	["goldbar",309000],
	["blastingcharge",10000],
	["boltcutter",75000],
	["defusekit",200],
	["storagesmall",75000],
	["storagebig",125000],
	["hammer",200],
	["emptywater",5],
	["puranium",750],
	["uranium1",1000],
	["uranium2",2000],
	["uranium3",3000],
	["uranium4",4000],
	["uranium",16400],
	["coal",600],
	["coalr",1760],
	["silver",800],
	["silverr",2360],
	["gold",890],
	["goldr",4360],
	["frog",50],
	["froglegs",999],
	["froglsd",3250],
	["metallschrott",25],
	["smara",500000],
	["silberschmuck",12360],
	["goldschmuck",14796],
	["weissgold",9995],
	["stahl",4620],
	["saft",150],
	["zinnu",380],
	["zinnp",3620],
	["bronze",7120],
	["fahrzeugteil",100],
	["rye",80],
	["hops",360],
	["yeast",100],
	["cornmeal",410],
	["moonshine",600],
	["bottles",700],
	["bottledshine",2560],
	["bottledbeer",1180],
	["bottledwhiskey",2258],
	["beerp",300],
	["whiskey",450],
	["mash",120],
	["corn",70],
	["bread",1000],
	["hen_raw",50],
	["rooster_raw",55],
	["goat_raw",80],
	["sheep_raw",400],
	["rabbit_raw",80],
	["hen_grilled",110],
	["rooster_grilled",160],
	["goat_grilled",460],
	["sheep_grilled",890],
	["rabbit_grilled",256],
	["strick",500],
	["gagkit",10000]
];

//███████████████████████████████████████████████████████████████████████
//██████████████████ DO NOT MODIFY THE FOLLOWING CODE! ██████████████████
//███████████████████████████████████████████████████████████████████████

DYNMARKET_Items_CurrentPriceArr = [];
DYNMARKET_sellarraycopy = DYNMARKET_Items_ToTrack;
DYNMARKET_Serveruptime = (DYNMARKET_Serveruptime * 3600) - 300;
{
	_currentArray = _x;
	DYNMARKET_Items_CurrentPriceArr pushBack [_currentArray select 0,_currentArray select 1,0];
} forEach DYNMARKET_Items_ToTrack;
publicVariable "DYNMARKET_UserNotification";
publicVariable "DYNMARKET_UserNotification_Text";
if (DYNMARKET_UseExternalDatabase) then {[1] call BBS_fnc_HandleDB;};
DYNMARKET_UpdateCount = 0;
if (DYNMARKET_UseExternalDatabase) then {
	[] spawn {
		sleep DYNMARKET_Serveruptime;
		diag_log "### DYNMARKET >> CURRENT PRICES ARE BEING WRITTEN TO THE DATABASE    ###";
		diag_log "### DYNMARKET >> AS PLANNED, AWAITING RESULT...                      ###";
		[0] call BBS_fnc_HandleDB;
	};
};
sleep 5;
[] call BBS_fnc_sleeper;
